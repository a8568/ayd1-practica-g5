import express from 'express';
import cors from 'cors';

const app = express();
const port = 4000;

// middlewares
app.use(cors());
app.use(express.urlencoded({ limit: '10mb', extended: true }));
app.use(express.json({ limit: '10mb'}));

app.use(express.static('public'));

app.use('/ping', (req, res) => {
    res.status(200).json({ message: 'pong' })
});


app.listen(port, () => console.log(`Server running on port ${port}`));