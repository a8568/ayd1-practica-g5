#!/bin/bash
set -eo pipefail
echo "Deployment"
filename="version"
while read -r line; do
    echo "$line" 

    docker run -it -d -p 80:80 --name movie-app  registry.gitlab.com/a8568/ayd1-practica-g5:movie-app${line}  
    docker run -it -d -p 4000:4000 --name movie-server  registry.gitlab.com/a8568/ayd1-practica-g5:movie-server${line}  

    echo "End Script"
done < "$filename"
