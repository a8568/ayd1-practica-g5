#!/bin/bash
set -eo pipefail
echo "Upload images to Gitlab"
filename="version"
while read -r line; do
    echo "$line" 

    docker tag movie-app registry.gitlab.com/a8568/ayd1-practica-g5:movie-app${line}
    docker tag movie-server registry.gitlab.com/a8568/ayd1-practica-g5:movie-server${line}

    docker push registry.gitlab.com/a8568/ayd1-practica-g5:movie-app${line}  
    docker push registry.gitlab.com/a8568/ayd1-practica-g5:movie-server${line}  

    echo "End Script"
done < "$filename"
